import React, { Component } from 'react';
import { connect } from 'react-redux';

import './CalcCanvas.css';
import InfoPanel from './components/InfoPanel';
import * as actions from './actions';
import { CALC_MENU } from './components/calcMenuConst';
import CalcMenuItem from './components/CalcMenuItem';

class CalcCanvas extends Component {
    constructor(props){
        super(props);
        const {state} = this.props;
        this.state = {
            show: false,
            clear: state.clear
        }
        this.handleClick = this.handleClick.bind(this);
    }
    sortMenuByActive() {
        const { state } = this.props;
        return [
            CALC_MENU[state.activeCalc],
            ...CALC_MENU.filter(item => item.index !== state.activeCalc)
        ]
    }

    handleClick(event){
        this.setState({
            show: !this.state.show
        })
    }

    componentDidUpdate() {
        const { state } = this.props;
        if (state.clear !== this.state.clear) {
            this.setState({
                clear: state.clear,
                show: false
            });
        }
    }


    render() {
        const { state } = this.props;
        const calcTitle = CALC_MENU[state.activeCalc].title;

        const sorted = this.sortMenuByActive();
        const menu = sorted.map((item, i) => <CalcMenuItem info={item} key={i + 1} />);

        return (
            <div className="App">
                <header className="header">
                    <div className='dropdown'>
                        <i className="fa fa-bars" onClick={this.handleClick}></i>
                    </div>
                    <h1 className="title">{calcTitle}</h1>
                </header>
                <InfoPanel menu={menu} show={this.state.show}/>
            </div>
        );
    }
}

export default connect((state) => ({ state: state }), actions)(CalcCanvas);
