const formInputs = {
    canvasSize: {
        id: 'canvasSize',
        title: 'Номер канвы (11, 14, 16, 18...)',
        sign: 'c'
    },
    widthPattern: {
        id: 'widthPattern',
        title: 'Длина схемы в крестиках',
        sign: 'w'
    },
    heightPattern: {
        id: 'heightPattern',
        title: 'Ширина схемы в крестиках',
        sign: 'h'
    },
    free: {
        id: 'free',
        title: 'Размер отступов, см',
        sign: ''
    },
    stringInCross: {
        id: 'stringInCross',
        title: 'Через сколько нитей вышивается крестик',
        sign: 's'
    },
    crossCount: {
        id: 'crossCount',
        title: 'Количество крестиков',
        sign: 'a'
    },
    moulineStringInCross: {
        id: 'moulineStringInCross',
        title: 'Во сколько нитей вышивается крестик',
        sign: 'm'
    },
    widthCanvas: {
        id: 'widthCanvas',
        title: 'Длина канвы в сантиметрах',
        sign: 'w'
    },
    heightCanvas: {
        id: 'heightCanvas',
        title: 'Ширина канвы в сантиметрах',
        sign: 'h'
    },

};

export const linenForm = [
    formInputs.canvasSize,
    formInputs.stringInCross,
    formInputs.widthPattern,
    formInputs.heightPattern,
    formInputs.free
];

export const linenFormTotal = 'cswh';

export const moulineForm = [
    formInputs.canvasSize,
    formInputs.crossCount,
    formInputs.moulineStringInCross 
];

export const moulineFormTotal = 'cam';

export const crossForm = [
    formInputs.canvasSize,
    formInputs.widthCanvas,
    formInputs.heightCanvas 
];

export const crossFormTotal = 'cwh';

export const aidaForm = [
    formInputs.canvasSize,
    formInputs.widthPattern,
    formInputs.heightPattern,
    formInputs.free 
];

export const aidaFormTotal = 'cwh';