import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class Input extends Component {
    constructor(props){
        super(props);
        const { state } = this.props;
        this.state = { 
            val: '', 
            clear: state.clear
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        const { state } = this.props;
        this.setState({
            val: event.target.value,
            clear: state.clear
        });
    }

    render() {
        const { info, state } = this.props;
        const val = this.state.clear !== state.clear ? '' : this.state.val;
        return (
           <input name={info.id} placeholder={info.title} id={info.sign} 
                    value={val}
                    onChange={this.handleChange} />
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Input)