import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class CalcMenuItem extends Component {
    handleClick(inx){
        const { chooseCalc } = this.props;
        chooseCalc(inx);
    }
    render() {
        const { state, info } = this.props;
        const isActive = state.activeCalc === info.index ? 'active': '';
        const cssItem = `calc-item ${isActive}`;
        return (
            <li className={cssItem} onClick={() => this.handleClick(info.index)}>
                <h3>{ info.title }</h3>
                <span>{ info.span }</span>
            </li>
        )
    }
}

export default connect((state) => ({state:state}), actions)(CalcMenuItem);