import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormCanvas from './FormCanvas';
import * as actions from '../actions.js';

import { LINEN, MOULINE, CROSS } from './calcMenuConst';
import {
    aidaForm, moulineForm,
    linenForm, crossForm,
    linenFormTotal, moulineFormTotal,
    crossFormTotal, aidaFormTotal
} from './inputConst';

class InfoPanel extends Component {
    getFields() {
        const { state } = this.props;
        switch (state.activeCalc) {
            case LINEN:
                return {
                    inputs: linenForm,
                    total: linenFormTotal
                };
            case MOULINE:
                return {
                    inputs: moulineForm,
                    total: moulineFormTotal
                };
            case CROSS:
                return {
                    inputs: crossForm,
                    total: crossFormTotal
                };
            default:
                return {
                    inputs: aidaForm,
                    total: aidaFormTotal
                };
        }
    }


    render() {
        const { state, menu, show } = this.props;

        let page = <div className='pnl-block '>
            <div className='calc-menu'>
                {menu}
            </div>
            <FormCanvas activeCalc={state.activeCalc} fields={this.getFields()} />
            </div>;
            
        if (show) {
            page = <div className='dropdown-menu'>
                {menu}
            </div>
        }

        return (
            page
        )
    }
}

export default connect((state) => ({ state: state }), actions)(InfoPanel);