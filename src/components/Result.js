import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';
import { MOULINE, CROSS, LINEN } from './calcMenuConst';

class Result extends Component {
    getResult(val){
        const { state } = this.props;
        switch (state.activeCalc){
            case MOULINE:
                return `Для вышивки вам потребуется нить мулине длиной ${val[0]} см, что меньше ${val[1]} мотка.`
            case CROSS:
                return `На ткани указанного размера можно вышить ${val[0]} x ${val[1]} крестиков`
            case LINEN:
                return `Для вышивки вам потребуется ткань размером ${val[0]} x ${val[1]} см.`
            default:
                return `Для вышивки вам потребуется ткань размером ${val[0]} x ${val[1]} см.`
        }
    }
    render() {
        const { val } = this.props;
        return (
            <div className='result'>
                {this.getResult(val)}
            </div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Result);