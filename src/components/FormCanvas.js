import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import Input from './Input';
import Result from './Result';
import { defineAida, defineCross, defineMouline, defineLinen } from '../calculate';
import { LINEN, MOULINE, CROSS } from './calcMenuConst';


class FormCanvas extends Component {
    constructor(props) {
        super(props);
        const { state } = props;
        this.state = {
            total: '',
            clear: state.clear
        };
        this.handleChange = this.handleChange.bind(this);
    }


    calculate() {
        const { activeCalc } = this.props;
        switch (activeCalc) {
            case LINEN:
                return defineLinen([
                    this.state.canvasSize,
                    this.state.stringInCross,
                    this.state.widthPattern,
                    this.state.heightPattern,
                    this.state.free
                ]);
            case MOULINE:
                return defineMouline([
                    this.state.canvasSize,
                    this.state.moulineStringInCross,
                    this.state.crossCount
                ]);
            case CROSS:
                return defineCross([
                    this.state.canvasSize,
                    this.state.widthCanvas,
                    this.state.heightCanvas
                ])
            default:
                return defineAida([
                    this.state.canvasSize,
                    this.state.widthPattern,
                    this.state.heightPattern,
                    this.state.free
                ])
        }
    }


    hasEmptyOrErrors(value) {
        return (value === '' ||
            /^\d+$/.test(value) === false ||
            parseInt(value, 10) <= 0 ||
            value.length > 5)
    }


    setTotal(total, char) {
        if (total.indexOf(char) > -1) {
            return total;
        }
        return total + char;
    }

    handleChange(event) {
        const sign = event.target.id;
        const total = this.hasEmptyOrErrors(event.target.value) ?
            this.state.total.replace(sign, '') :
            this.setTotal(this.state.total, sign);
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value,
            total: total
        });
    }


    componentWillUpdate() {
        const { state } = this.props;
        if (state.clear !== this.state.clear) {
            this.setState({
                clear: state.clear,
                free: '',
                total: ''
            });
        }
    }


    render() {
        const { fields } = this.props;
        let inputs = fields.inputs.map((item, inx) =>
            <Input key={inx} info={item} />);

        let result = null;
        if (fields.total === this.state.total) {
            const value = this.calculate();
            result = <Result val={value} />
        }
        return (
            <div>
                <form onChange={this.handleChange}>
                    {inputs}
                </form>
                {result}
            </div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(FormCanvas);