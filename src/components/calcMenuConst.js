export const AIDA = 0;

export const LINEN = 1;

export const MOULINE = 2;

export const CROSS = 3;

export const CALC_MENU = [
    {
        index: AIDA,
        title: 'Калькулятор канвы Aida',
        span: 'Считает количество канвы Аида для вышивки'
    },    
    {
        index: LINEN,
        title: 'Калькулятор для ткани равномерного переплетения',
        span: 'Считает количество ткани равномерного переплетения для вышивки'
    },    
    {
        index: MOULINE,
        title: 'Калькулятор мулине',
        span: 'Считает количество мулине для вышивки'
    },    
    {
        index: CROSS,
        title: 'Калькулятор крестиков',
        span: 'Определяет, сколько крестиков займет вышивка'
    },    
];    
