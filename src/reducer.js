import { AIDA } from "./components/calcMenuConst";

const init = () => {
    return {
        activeCalc: AIDA,
        clear: 0
    }
}

const chooseCalc = (state, inx) => {
    return{
        ...state,
        activeCalc: inx,
        clear: state.clear + 1
    }
}

export default (state = init(), action) => {
    switch (action.type){
        case 'choose': return chooseCalc(state, action.inx);
        default: return state;
    }
}
