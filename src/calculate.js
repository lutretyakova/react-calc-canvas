const INCH = 2.54;
const HANK_LENGHT = 4800; // sm

export const defineAida = (data) => {
    const [canvas, width, height, free] = data.map(item => parseInt(item, 10) || 0);
    const crossInOneSm = canvas / INCH;
    return [
        Math.ceil(width / crossInOneSm + 2 * free), 
        Math.ceil(height / crossInOneSm + 2 * free)
    ]
}


export const defineCross = (data) => {
    const [canvas, width, height] = data.map(item => parseInt(item, 10));
    const crossInOneSm = canvas / INCH;
    return [
        Math.floor(width * crossInOneSm), 
        Math.floor(height * crossInOneSm)
    ]
}


export const defineLinen = (data) => {
    const [canvas, strInCross, width, height, free] = data.map(item => parseInt(item, 10) || 0);
    const crossInOneSm = canvas / (strInCross * INCH);
    return [
        Math.ceil(width / crossInOneSm + 2 * free), 
        Math.ceil(height / crossInOneSm + 2 * free)
    ]
}

export const defineMouline = (data) => {
    const [canvas, strInCross, crossCount] = data.map(item => parseInt(item, 10));
    const crossHeight = INCH / canvas;
    const crossDiag = Math.hypot(crossHeight, crossHeight);

    const crossLenght = 2 * (crossHeight + crossDiag) * crossCount * strInCross;
    
    return [
        crossLenght.toFixed(2), 
        Math.ceil(crossLenght / HANK_LENGHT)
    ]
}