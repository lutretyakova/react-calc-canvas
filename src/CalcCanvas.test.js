import React from 'react';
import ReactDOM from 'react-dom';
import CalcCanvas from './CalcCanvas';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducer';

const store = createStore(reducer);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}>
    <CalcCanvas />
  </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});


