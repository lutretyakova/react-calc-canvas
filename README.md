# Набор калькуляторов для вышивки крестом:

* Калькулятор для канвы Аида
* Калькулятор для тканей равномерного переплетения или льна
* Калькулятор крестиков
* Калькулятор мулине.

# Блог
https://lutretyakova.gitlab.io/react-calc-canvas.html

# Demo
[Посчитать](https://lutretyakova.gitlab.io/projects/react-calc-canvas/)




